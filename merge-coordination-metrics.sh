#!/bin/sh

#shellcheck disable=SC3037

# Usage: ./merge-coordination-metrics.sh

set -eu

PROJECT_ID=tezos%2Ftezos

issues() {
    curl -s "https://gitlab.com/api/v4/projects/$PROJECT_ID/issues_statistics?$1"
}

open_count() {
    jq -r .statistics.counts.opened
}

all_open=$(issues "" | open_count)

echo "Metric,$(date +%Y-%b-%d)"

echo -n "tezos/tezos open unassigned issues,"
issues "assignee_id=None" | open_count

echo -n "tezos/tezos open bugs,"
issues "labels=type::bug" | open_count

echo -n "tezos/tezos open unassigned bugs,"
issues "labels=type::bug&assignee_id=None" | open_count

echo -n "tezos/tezos issues not in projects,"
issues "milestone=None" | open_count

prioritized=0
for p in critical high low medium; do
    has_p=$(issues "labels=priority::$p" | open_count)
    prioritized=$((prioritized + has_p))
done
echo -n "tezos/tezos unprioritized issues,"
echo $((all_open - prioritized))

echo -n "tezos/tezos open merge requests,"
curl -I -s "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests?per_page=1&state=opened"  | grep 'x-total:' | cut -d':' -f2
