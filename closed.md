# Closed

MRs that were actice at some point and that got closed because of
our actions.

- tezos/tezos!3754 - `lib_stdlib_unix: add fmt to dune dependencies`, by @bikallem
  - Nov 3rd: fix assignee, set reviewers
- tezos/tezos!3674 - `refactor bin_validator in a binary and library component`, by @abate
  - Nov 8th: pinged @arvidnl about a pending thread, asked why @pirbo is assigned
   despite the two approvals already.
- tezos/tezos!3302 - `Fix 404 error in protocol RPC when asked for contract's delegate`, by @sventimir
  - Oct 22th: it was closed by the author
  - Oct 21th: apparently we don't want to merge this, so suggested to close.
  - Oct 15th: Asked for clarification of whether we want to merge this
  - Oct 13: Waiting review from @romain.nl since a while, changed assigned to @shrmtv
- tezos/tezos!2964 - `Proto: Improve Raw_context storage space errors`
  - Sept 22: set it as draft as MR author has been inactive despite multiple
    pings. Sad because it has one approval already.
- tezos/tezos!2571 - `Proto: Refactoring of token movements and associated update logs (token management))`, by @bsall
  - Sept 23: @bsall closed.
  - Sept 22: @bsall is wai, following our advice of not bothering @mbouaziz
    closing the MR. As this looks useless work, asked @bsall if this is really required.
- tezos/tezos!2352 - `Separate the michelson interpreter from the protocol`, by @theeduardorfs
  - Oct 21th: I closed it
  - Oct 15th: pinged the author.
  - Sept: ping by @lthms
  - Stalling for 5 months
- tezos/tezos!2043 - `Patch release 7.3 for freebsd`, by @webaxter
  - Oct 5th, author closed it
  - Oct 4th, pinged author
