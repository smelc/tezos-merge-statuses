# tezos-merge-statuses

Repo to keep track of the actions done by [@rafoo\_](https://gitlab.com/smelc)
and [@smelc](https://gitlab.com/smelc) as part of [Merge Stream Speedup](https://gitlab.com/tezos/tezos/-/milestones/41).
Later replaced by [@sventimir](https://gitlab/sventimir) as the [Merge Coordinator](https://tezos.gitlab.io/developer/merge_team.html?highlight=merge%20team#merge-coordinator).

This repo does not follow a merge request workflow. Push on `main` directly.
Do merge-commits if you want, or else rebase your changes.

Main files:

- [active.org](https://gitlab.com/smelc/tezos-merge-statuses/-/blob/main/active.org) lists MRs currently being supported
- [merged.org](https://gitlab.com/smelc/tezos-merge-statuses/-/blob/main/merged.org) lists MRs successfully merged with our support
- [closed.org](https://gitlab.com/smelc/tezos-merge-statuses/-/blob/main/closed.org) lists MRs closed after our intervention

Deprecated files:

- [active.md](https://gitlab.com/smelc/tezos-merge-statuses/-/blob/main/active.md) [DEPRECATED] lists the MRs that are currently being supported.
- [merged.md](https://gitlab.com/smelc/tezos-merge-statuses/-/blob/main/merged.md) [DEPRECATED] lists the MRs on which we interveened and which have been merged. Serves as a history of our actions and will be useful to see what kind of actions we did, and how they worked.

How we find work:

- We inspect the list of [opened and non-draft MRs](https://gitlab.com/tezos/tezos/-/merge_requests?draft=no&scope=all&sort=created_asc&state=opened) on [tezos/tezos](https://gitlab.com/tezos/tezos).

What we do:

- We add reviewers when there are less than two
- We fix the [Assignee](https://tezos.gitlab.io/developer/contributing.html#merge-request-assignees-field)
  field when it is wrong, explaining why in a comment and we link to this doc
  to show how this field must be used.
- We set as _draft_:
  - MRs that receives 3 pings/nudges and no action was taken.
  - MRs that are far from being in a state where review is possible.

History: we keep track of the number of opened and non-draft MRs, to have
a measure of our effect, it's the number of _Open_ MRs as returned
by the search for [opened and non-draft MRs](https://gitlab.com/tezos/tezos/-/merge_requests?draft=no&scope=all&sort=created_asc&state=opened).

- Dec 3rd: 47 MRs
- Dec 2nd: 44 MRs
- Dec 1st: 50 MRs
- Nov 30: 50 MRs
- Nov 29: 54 MRs
- Nov 26: 59 MRs
- Nov 24: 61 MRs
- Nov 22: 64 MRs
- Nov 22: 52 MRs
- Nov 19: 48 MRs
- Nov 18: 39 MRs
- Nov 17: 41 MRs
- Nov 16: 44 MRs
- Nov 15: 45 MRs
- Nov 12: 46 MRs
- Nov 10: 40 MRs
- Nov 8: 32 MRs
- Nov 5: 36 MRs
- Nov 3: 38 MRs
- Nov 2: 41 MRs
- Oct 22: 40 MRs
- Oct 21: 49 MRs
- Oct 20: 46 MRs
- Oct 19: 49 MRs
- Oct 18: 50 MRs
- Oct 15: 41 MRs
- Oct 14: 42 MRs
- Oct 13: 42 MRs
- Oct 12: 49 MRs
- Oct 11: 53 MRs
- Oct 8: 45 MRs
- Oct 7: 42 MRs
- Oct 6: 51 MRs
- Sept 30: 54 MRs
- Sept 27: 53 MRs

# Installation

To update the file `active.org`, you need to use org-mode in Emacs
with python enabled in babel, an appropriate pyenv with the python
dependencies installed. Finally, you have to configure `python-gitlab`
and add your GitLab API access token.

To enable the evaluation of python source blocks in org-mode, add the
following to your Emacs configuration:

```elisp
(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)))
```

To create the python environment called `merge-coordination` using
Python 3.9.5, run:

```shell
pyenv virtualenv 3.9.5 merge-coordination
```

To install the python dependencies (namely, `python-gitlab`), run
`poetry install`.

Now, create the file `gl.cfg` in this folder by copying the template
`gl.cfg.bak`:

```shell
cp gl.cfg.EXAMPLE gl.cfg
````

Fill it out by adding [your access
token](https://gitlab.com/-/profile/personal_access_tokens) of scope
`read_api`.

At this point, you should be able to run the source block at the end
of `active.org`.
